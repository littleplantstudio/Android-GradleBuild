/*
Usage example:

apply from: "${rootDir}/libraries/GradleBuild/commons.gradle"

android {
    defaultConfig {
        ...
        setupVersion it, '1.0.0'
    }
    ...
    productFlavors {
        dev {
            setStringField it, 'SERVER_URL', 'http://example.com'
            setStringRes it, 'flurry_key', 'abcdefghijklmnoprst'
        }
    }
    ...
    setOutputFileName it, 'appname'
}
 */


/**
 * Adds string field to the generated BuildConfig class.
 *
 * Can be called within defaultConfig, build type or flavor initialization closure:
 * "setStringField it, 'NAME', 'VALUE'".
 */
ext.setStringField = { obj, key, value ->
    obj.buildConfigField 'String', key, "\"${value}\""
}

/**
 * Adds string resource to be accessible as R.string.key.
 *
 * Can be called within defaultConfig, build type or flavor initialization closure:
 * "setStringRes it, 'name', 'value'".
 */
ext.setStringRes = { obj, key, value ->
    obj.resValue 'string', key, value
}

/**
 * Whether we are running on build server currenly or not
 */
ext.isOnBuildServer = {
    return System.getenv('BUILD_NUMBER')
}

/**
 * Sets app version name and version code.
 *
 * Provided version should be in form 'X.Y.Z', these version will be parsed and version code will be
 * generated from it as 'X * 10000 + Y * 100 + Z', i.e. '1.2.1' will be converted to '10201'.
 *
 * When building on build server version name will be extended to contain build number,
 * i.e. '1.2.1-b231'. For svn it will be latest svn revision number, for git it will be
 * a total number of commits from the branch root.
 *
 * Should be called instead of 'versionName' / 'versionCode' within defaultConfig closure:
 * "setupVersion it, 'X.Y.Z'".
 */
ext.setupVersion = { config, version ->
    def build = null

    if (isOnBuildServer()) {
        // We are running on Jenkins server
        build = System.getenv('SVN_REVISION')
        if (!build) build = 'git rev-list --count HEAD'.execute().text.trim()
    }

    def (major, minor, patch) = version.tokenize('.')
    def code = major.toInteger() * 10000 + minor.toInteger() * 100 + patch.toInteger()
    def name = build ? "${version}-b${build}" : version

    config.versionCode code
    config.versionName name

    println "-------- VERSION --------\n" +
            "-- CODE = ${code}\n" +
            "-- NAME = ${name}\n" +
            "-------------------------"
}

/**
 * Sets better apk file name using format: "'appName'-'buildType'-'flavor'-'versionName'.apk".
 *
 * Should be called within android setup, closer to the end: "setOutputFileName it, 'appName'".
 */
ext.setOutputFileName = { appName ->
    android.applicationVariants.all { variant ->
        variant.outputs.each { output ->
            String apkName = "${appName}-${output.baseName}-${variant.versionName}"
            String dir = output.outputFile.parent

            if (output.packageApplication) {
                if (output.zipAlign) {
                    output.packageApplication.outputFile = new File(dir, "${apkName}.unaligned_apk")
                    output.zipAlign.inputFile = output.packageApplication.outputFile
                    output.zipAlign.outputFile = new File(dir, "${apkName}.apk")
                } else {
                    output.packageApplication.outputFile = new File(dir, "${apkName}.apk")
                }
            }
        }
    }
}


/**
 * Disabling pre-dexing to speed up build server
 */
if (isOnBuildServer()) {
    rootProject.afterEvaluate {
        rootProject.allprojects { p ->
            if (p.hasProperty('android') && p.android) {
                p.android.dexOptions.preDexLibraries = false
                println "Disabling pre dex for ${p.name}"
            }
        }
    }
}

/*
 * Creates build task 'ci' to use with Jenkins.
 * It expects MODE build parameter (provided with -DMODE=XXX by Jenkins) to be set to flavor name.
 * This task will assemble, check and upload debug build to Fabric for given flavor.
 */
project.afterEvaluate {
    String mode = System.properties.MODE
    String modeName = mode ? mode.toLowerCase().capitalize() : ''
    modeName = modeName.replaceAll(/_\w/) { it[1].toUpperCase() } // Underscores to camel case

    final buildTask = project.task 'ci'

    buildTask.dependsOn 'check'

    final assembleTask = project.tasks["assemble${modeName}Debug"]
    buildTask.dependsOn assembleTask

    final distTaskName = "crashlyticsUploadDistribution${modeName}Debug"
    if (project.tasks.asMap.containsKey(distTaskName)) {
        final distTask = project.tasks[distTaskName]
        buildTask.dependsOn distTask
        distTask.dependsOn assembleTask
    }
}
